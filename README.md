# Utah All-Payer Claims Database (APCD) 
## Data Submission Guide

### Current Version (Effective as of March 1, 2020)
[Version 4](https://gitlab.com/UtahOHCS/APCD_DSG/blob/4.0/APCD_UT_DSG.md)

### Previous Version
[Version 3.1.1 (AKA 3.1 "CORRECTED")](https://gitlab.com/UtahOHCS/APCD_DSG/blob/3.1.1/APCD_UT_DSG.md)

### Changelog
[CHANGELOG.md](./CHANGELOG.md)